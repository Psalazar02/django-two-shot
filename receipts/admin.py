from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
        "owner",
    ]

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
        "number",
    ]

@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = [
        "vendor",
        "purchaser",
        "id",
    ]
